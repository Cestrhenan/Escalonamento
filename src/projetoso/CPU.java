package projetoso;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class CPU {
    
    
    public static void main(String[] args) {

        ArrayList<Process> processos = new ArrayList<>();
        int idprocesso = 0, tempo_chegada = 0, duracao = 0;

        try {
            Scanner in = new Scanner(new File("/home/rhenan/Área de Trabalho/projetoso/SO"));
            while (in.hasNextLine()) {
                String aux = in.nextLine();
                String[] aux2 = aux.split(" ");
                tempo_chegada = Integer.parseInt(aux2[0]);
                duracao = Integer.parseInt(aux2[1]);
                //System.out.println(tempo_chegada);
                //System.out.println(duracao);
                processos.add(new Process(++idprocesso, tempo_chegada, duracao));
            }

            int i = 0;
            Iterator it = processos.iterator();
            while (it.hasNext()) {
                //System.out.println(processos.get(i));
                it.next();
                i++;
            }

        } catch (Exception e) {
            System.err.println(e);
        }

        FirstComeFirstService fcfs = new FirstComeFirstService(processos);
        ShortestJobFirst sjf = new ShortestJobFirst((List<Process>) processos.clone());
        RoundRobin rr = new RoundRobin(processos);
       

        fcfs.valoresFCFS();
        sjf.valoresSJF();
        rr.valoresRR();

    }   
    
    //devolve o menor tempo de chegada da lista
    public int tempodeChegadaMinimo(List<Process> processos) {
        int tminimo = Integer.MAX_VALUE; //2³¹
        for (Process p : processos) {
            if (p.getTempodeChegada() < tminimo) {
                tminimo = p.getTempodeChegada();
            }
        }
        return tminimo; 
    }

   
}
