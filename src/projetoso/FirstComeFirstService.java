package projetoso;

import java.awt.Event;
import java.util.ArrayList;
import java.util.List;
import static java.lang.String.format;
import static java.lang.String.format;

public class FirstComeFirstService extends CPU {

    private static List<Process> listaProntos;
    private float retornoMedio;
    private float respostaMedio;
    private float esperaMedio;

    public FirstComeFirstService(List<Process> processos) {
        
        
        try {
            listaProntos = new ArrayList<Process>(processos);
            int tempoRetorno = 0, tempoResposta = 0, tempoEspera = 0;
            int totalProcessos = processos.size();
            int retorno = tempodeChegadaMinimo(processos);

            // enquanto tiver processo, remove o processo, incrementa duração no retorno e calcula
            //tempo de retorno e tempo de espera
            while (!listaProntos.isEmpty()) {
                Process p = listaProntos.remove(0);
                retorno += p.getDuracao();
                tempoRetorno += (retorno - p.getTempodeChegada());
                tempoEspera += (retorno - p.getTempodeChegada() - p.getDuracao());                
            }tempoResposta = tempoEspera; //tempo de resposta é o tempode de espera, no caso, o ultimo processo vai responder 
                                          //depois de esperar todos os outros processos executarem

            // seta os valores de saída
            retornoMedio = ((float) tempoRetorno / totalProcessos);
            respostaMedio = ((float) tempoResposta / totalProcessos);
            esperaMedio = ((float) tempoEspera / totalProcessos);
            
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void valoresFCFS() {
        System.out.println(format("FCFS: %.1f %.1f %.1f", retornoMedio, respostaMedio, esperaMedio));
    }
}
