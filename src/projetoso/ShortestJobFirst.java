package projetoso;

import java.util.ArrayList;
import java.util.List;
import static java.lang.String.format;
import java.util.Collections;
import java.util.Iterator;


public class ShortestJobFirst extends CPU {

    private float retornoMedio;
    private float respostaMedio;
    private float esperaMedio;
    List<Process> listadeProntos = new ArrayList<Process>();


    public ShortestJobFirst(List<Process> processos) {
        
        try {
            int tempodeRetorno = 0, tempodeResposta = 0, tempodeEspera = 0;
            int totaldeProcessos = processos.size();
            int retorno = tempodeChegadaMinimo(processos);
            Ordena(processos);

            // Pega todos os processos e remove um a um da fila
            // atribuindo os valores de cada um a retorno, tempo de retorno e tempo de espera
            // o tempo de espera é definido por TEspera = Tretorno - TChegada - TDuracao
            //Algoritmo FCFS
            while (!listadeProntos.isEmpty()) {
                Process p = listadeProntos.remove(0);
                retorno += p.getDuracao();
                tempodeRetorno += (retorno - p.getTempodeChegada());
                tempodeEspera += (retorno - p.getTempodeChegada() - p.getDuracao());
            }
            tempodeResposta = tempodeEspera;

            // calcula os valores
            retornoMedio = ((float) tempodeRetorno / totaldeProcessos);
            respostaMedio = ((float) tempodeResposta / totaldeProcessos);
            esperaMedio = ((float) tempodeEspera / totaldeProcessos);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    private void Ordena(List<Process> processos) {
        int retorno = 0, menor = 0, aux = 0;
        //Collections.sort(processos);
      //  System.out.println("peguei o: " +processos.get(0));
        listadeProntos.add(processos.remove(0));
        retorno = listadeProntos.get(0).getDuracao();
//        for (int y = 0; y<processos.size(); y++) {
//            System.out.println(processos.get(y));
        
//}
        //vai ordenar os processos de acordo com duração e chegada  
        while (processos.size() > 1) {
            aux = 0;
            for (int i = 1; i < processos.size(); i++) {
                if (processos.get(i).getDuracao() >= processos.get(aux).getDuracao() 
                        && retorno > processos.get(aux).getTempodeChegada()) {
                    menor = aux;
                } else if (retorno > processos.get(i).getTempodeChegada()) {
                    aux = i;
                    menor = i;
                }
            }
            retorno += processos.get(menor).getDuracao();
            listadeProntos.add(processos.remove(menor));
        }
        //pega o processo remanescente e coloca na fila
        if (processos.size() == 1) {
            listadeProntos.add(processos.remove(0));
        }
    }

    public void valoresSJF() {
        System.out.println(format("SJF: %.1f %.1f %.1f", retornoMedio, respostaMedio, esperaMedio));
    }

}
