package projetoso;

//Processo Implementa Comparable para Uso em SJF
public class Process implements Comparable<Process> {

    private int id;
    private int tempodeChegada;
    private int duracao;
    private int duracaoRestante; // so vai ser usado no sjf

    public Process(int id, int tempodeChegada, int duracao) {
        this.id = id;
        this.tempodeChegada = tempodeChegada;
        this.duracao = duracao;
        this.duracaoRestante = duracao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTempodeChegada() {
        return tempodeChegada;
    }

    public void setTempodeChegada(int tempoChegada) {
        this.tempodeChegada = tempodeChegada;
    }

    public int getDuracao() {
        return duracao;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }

    public int getDuracaoRestante() {
        return duracaoRestante;
    }

    public void setDuracaoRestante(int duracaoRestante) {
        this.duracaoRestante = duracaoRestante;
    }

    //Implementação da Interface Comparable
    //para o Shortest Job First para ordenar os processos
    @Override
    public int compareTo(Process p) {
        if ((p.getTempodeChegada() <= this.tempodeChegada) && p.getDuracao() < this.getDuracao()) {
            return 1;
        } else if ((p.getTempodeChegada() >= this.tempodeChegada) && (p.getDuracao() > this.getDuracao())) {
            return -1;
        } else {
            return 0;
        }
    }


 public String toString () {
 
    return "Tempo de Chegada: " +getTempodeChegada()+ " Duração: " +getDuracao(); 
    
    
    }

}