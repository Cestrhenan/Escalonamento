package projetoso;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import static java.lang.String.format;



public class RoundRobin extends CPU {
    
    private float retornoMedio;
    private float respostaMedio;
    private float esperaMedio;
    List<Process> listadeProntos = new ArrayList<Process>();
    List<Integer> tempodeChegada = new ArrayList<Integer>();
    Map<Integer, Integer> tempodeResposta = new HashMap<Integer, Integer>();
    int quantum = 2;

    public RoundRobin(List<Process> processos) {
        int tempodeRetorno = 0, tempodeEspera = 0, tempoResposta = 0;
        int totaldeProcessos = processos.size();
        int retorno = tempodeChegadaMinimo(processos);
        ListadeProntos(processos, retorno);

        //Enquanto a Lista de Prontos estiver com processos o while é executado
        //A cada processo na lista, um valor é setado no mapa onde o ID é o mesmo 
        //do processo e o valor é tempo de resposta (retorno - tempo de chegada) 
        //daquele processo
        while (!listadeProntos.isEmpty()) {
            Process p = listadeProntos.remove(0);
            if (!tempodeResposta.containsKey(p.getId())) {
                tempodeResposta.put(p.getId(), (retorno - p.getTempodeChegada()));
                //campo preenchido
            }
            // se o tempo de duração for maior do que o valor definido do quantum
            // o valor do quantum é subtraido da duração e o retorno
            // recebe o valor do quantum. Chama a lista de prontos passando 
            //os valores e por fim, adiciona o processo ao final da lista
            if (p.getDuracaoRestante() > quantum) {
                p.setDuracaoRestante(p.getDuracaoRestante() - quantum);
                retorno += quantum;
                ListadeProntos(processos, retorno);
                // adiciona o processo no final da lista, pois não foi finalizado
                listadeProntos.add(p);
            } else {
                retorno += p.getDuracaoRestante();
            }

            //se o processo for finalizado os de retorno e espera totais são calculados
            if (!listadeProntos.contains(p)) {
                tempodeRetorno += retorno - p.getTempodeChegada();
                tempodeEspera += (retorno - p.getTempodeChegada() - p.getDuracao());
            }
        }
        //soma o tempo de todos os processos para calcular o tempo de resposta medio
        for (int key : tempodeResposta.keySet()) {
            tempoResposta += tempodeResposta.get(key);
        }
        //depois da execução de todos os processos os valores são calculados
        retornoMedio = ((float) tempodeRetorno / totaldeProcessos);
        respostaMedio = ((float) tempoResposta / totaldeProcessos);
        esperaMedio = ((float) tempodeEspera / totaldeProcessos);
    }
    
    //Para cada processo, se o tempo de execução for maior que o quantum 
    //ele é colocado de volta no fim da lista de prontos
    
    private void ListadeProntos(List<Process> processos, int retorno) {
        int menor_chegada = 0;
        for (Process p : processos) {
            if (retorno >= p.getTempodeChegada() && !tempodeChegada.contains(p.getTempodeChegada())) {
                if (!listadeProntos.contains(p)) {
                    menor_chegada = p.getTempodeChegada();
                    tempodeChegada.add(menor_chegada);
                    for (Process processo : processos) {
                        if (processo.getTempodeChegada() == menor_chegada) {
                            listadeProntos.add(processo);
                        }
                    }
                }
            }
        }
    }

    public void valoresRR() {
        System.out.println(format("RR: %.1f %.1f %.1f", retornoMedio, respostaMedio, esperaMedio));
    }
}
